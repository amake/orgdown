★ [[../Tool-Support.org][← Back to Tool Support]] ★ [[../Overview.org][↑ Overview page]] ★

* Orgdown support for Orgro

- Platform: Android, iOS
- License: GPL-3.0
- Price: US$4.99
- [[https://orgro.org][Homepage]]
- [[https://orgro.org/faq/][FAQ]]
- [[https://github.com/amake/orgro][Source code]] and [[https://github.com/amake/orgro/issues][issue management]]
- last Orgdown1 syntax assessment: 2021-12-10 using version 1.23.0 by [[https://github.com/amake][Aaron Madlon-Kay]]

| *Percentage of Orgdown1 syntax support:* | 88 |
#+TBLFM: @1$2=(remote(toolsupport,@>$2)/86)*100;%.0f

** Details

- legend:
  - *1* → works as simple text
  - *2* → at least one of:
    - syntax highlighting
    - support for inserting/collapse/expand/…
    - support for toggling (e.g., checkboxes, states, …)

#+NAME: toolsupport
| Element                                                                     | OD1 | OD1 comment                     |
|-----------------------------------------------------------------------------+-----+---------------------------------|
| syntax elements do not need to be separated via empty lines                 |   2 |                                 |
|-----------------------------------------------------------------------------+-----+---------------------------------|
| *bold*                                                                      |   2 |                                 |
| /italic/                                                                    |   2 |                                 |
| _underline_                                                                 |   2 |                                 |
| +strike through+                                                            |   2 |                                 |
| =code=                                                                      |   2 |                                 |
| ~commands~                                                                  |   2 |                                 |
| Combinations like *bold and /italic/ text*                                  |   2 |                                 |
| : example                                                                   |   2 |                                 |
| [[https://example.com][link *bold* test]]                                                            |   1 |                                 |
| [[https://example.com][link /italic/ test]]                                                          |   1 |                                 |
| [[https://example.com][link +strike through+ test]]                                                  |   1 |                                 |
| [[https://example.com][link =code= test]]                                                            |   1 |                                 |
| [[https://example.com][link ~commands~ test]]                                                        |   1 |                                 |
| ≥5 dashes = horizontal bar                                                  |   1 |                                 |
|-----------------------------------------------------------------------------+-----+---------------------------------|
| Headings using asterisks                                                    |   2 |                                 |
|-----------------------------------------------------------------------------+-----+---------------------------------|
| Unordered list item with "-"                                                |   2 |                                 |
| Ordered list item with 1., 2., …                                            |   2 |                                 |
| Checkbox + unordered list item with "-"                                     |   1 |                                 |
| Checkbox + ordered list item with 1., 2., …                                 |   1 |                                 |
| Mixed lists of ordered and unordered items                                  |   2 |                                 |
| Multi-line list items with collapse/expand                                  |   1 |                                 |
| Multi-line list items with support for (auto-)indentation                   |   2 |                                 |
|-----------------------------------------------------------------------------+-----+---------------------------------|
| Example block                                                               |   2 |                                 |
| Quote block                                                                 |   2 |                                 |
| Verse block                                                                 |   1 |                                 |
| Src block                                                                   |   2 | Highlighting for many languages |
|-----------------------------------------------------------------------------+-----+---------------------------------|
| Comment lines with "# <foobar>"                                             |   2 |                                 |
| Comment block                                                               |   2 |                                 |
|-----------------------------------------------------------------------------+-----+---------------------------------|
| https://gitlab.com/publicvoit/orgdown plain URL without brackets            |   2 |                                 |
| [[https://gitlab.com/publicvoit/orgdown]] URL with brackets without description |   2 |                                 |
| [[https://gitlab.com/publicvoit/orgdown][OD homepage]] URL with brackets with description                              |   2 |                                 |
|-----------------------------------------------------------------------------+-----+---------------------------------|
| Basic table functionality                                                   |   2 |                                 |
| Table cells with *bold*                                                     |   2 |                                 |
| Table cells with /italic/                                                   |   2 |                                 |
| Table cells with _underline_                                                |   2 |                                 |
| Table cells with +strike through+                                           |   2 |                                 |
| Table cells with =code=                                                     |   2 |                                 |
| Table cells with ~commands~                                                 |   2 |                                 |
| Auto left-aligning of text                                                  |   2 |                                 |
| Auto right-aligning of numbers like "42.23"                                 |   2 |                                 |
|-----------------------------------------------------------------------------+-----+---------------------------------|
| Other syntax elements are interpreted at least as normal text (or better)   |   2 | Inline & block LaTeX rendering  |
| Other syntax elements: linebreaks respected (or better)                     |   2 | Text reflowing                  |
|-----------------------------------------------------------------------------+-----+---------------------------------|
| *Sum*                                                                       |  76 |                                 |
#+TBLFM: @>$2=vsum(@I$2..@>>$2)
