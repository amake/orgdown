★ [[../Tool-Support.org][← Back to Tool Support]] ★ [[../Overview.org][↑ Overview page]] ★

* Orgdown support for Logseq

- Platform: Win/macOS/Linux
- License: GNU Affero General Public License v3.0
- Price: Free
- https://logseq.com/
- https://docs.logseq.com/#/page/Contents
- https://github.com/logseq/logseq/issues/, https://discuss.logseq.com/c/bug-reports, https://discuss.logseq.com/c/feature-requests
- last Orgdown1 syntax assessment: 2024-01-21 using version 0.10.5 by Karl Voit

| *Percentage of Orgdown1 syntax support:* | 86 |
#+TBLFM: @1$2=(remote(toolsupport,@>$2)/86)*100;%.0f

** Details

- legend:
  - *1* → works as simple text
  - *2* → at least one of:
    - syntax highlighting
    - support for inserting/collapse/expand/…
    - support for toggling (e.g., checkboxes, states, …)

#+NAME: toolsupport
| Element                                                                     | OD1 | OD1 comment                                                                                                 |
|-----------------------------------------------------------------------------+-----+-------------------------------------------------------------------------------------------------------------|
| syntax elements do not need to be separated via empty lines                 |   2 |                                                                                                             |
|-----------------------------------------------------------------------------+-----+-------------------------------------------------------------------------------------------------------------|
| *bold*                                                                      |   2 |                                                                                                             |
| /italic/                                                                    |   2 |                                                                                                             |
| _underline_                                                                 |   2 |                                                                                                             |
| +strike through+                                                            |   2 |                                                                                                             |
| =code=                                                                      |   2 |                                                                                                             |
| ~commands~                                                                  |   2 |                                                                                                             |
| Combinations like *bold and /italic/ text*                                  |   2 |                                                                                                             |
| : example                                                                   |   2 |                                                                                                             |
| [[https://example.com][link *bold* test]]                                                            |   2 |                                                                                                             |
| [[https://example.com][link /italic/ test]]                                                          |   2 |                                                                                                             |
| [[https://example.com][link +strike through+ test]]                                                  |   2 |                                                                                                             |
| [[https://example.com][link =code= test]]                                                            |   1 | It's rendered literally showing = surrounding the word within the link                                      |
| [[https://example.com][link ~commands~ test]]                                                        |   2 |                                                                                                             |
| ≥5 dashes = horizontal bar                                                  |   1 | 5 dashes exactly produces a horizontal bar, more turns into that number of dashes                           |
|-----------------------------------------------------------------------------+-----+-------------------------------------------------------------------------------------------------------------|
| Headings using asterisks                                                    |   1 | Headings are defined independent of their asterisk level and stored as properties                           |
|-----------------------------------------------------------------------------+-----+-------------------------------------------------------------------------------------------------------------|
| Unordered list item with "-"                                                |   2 |                                                                                                             |
| Ordered list item with 1., 2., …                                            |   2 |                                                                                                             |
| Checkbox + unordered list item with "-"                                     |   2 |                                                                                                             |
| Checkbox + ordered list item with 1., 2., …                                 |   0 |                                                                                                             |
| Mixed lists of ordered and unordered items                                  |   2 |                                                                                                             |
| Multi-line list items with collapse/expand                                  |   0 |                                                                                                             |
| Multi-line list items with support for (auto-)indentation                   |   0 |                                                                                                             |
|-----------------------------------------------------------------------------+-----+-------------------------------------------------------------------------------------------------------------|
| Example block                                                               |   2 | Caption keyword does not work.                                                                              |
| Quote block                                                                 |   2 | Caption keyword does not work.                                                                              |
| Verse block                                                                 |   2 | Caption keyword does not work.                                                                              |
| Src block                                                                   |   2 | Caption keyword does not work. Highlights available for some languages if language provided after begin_src |
|-----------------------------------------------------------------------------+-----+-------------------------------------------------------------------------------------------------------------|
| Comment lines with "# <foobar>"                                             |   2 | Comments are not rendered unless you are editing that block                                                 |
| Comment block                                                               |   2 | Comments are not rendered unless you are editing that block                                                 |
|-----------------------------------------------------------------------------+-----+-------------------------------------------------------------------------------------------------------------|
| https://gitlab.com/publicvoit/orgdown plain URL without brackets            |   2 |                                                                                                             |
| [[https://gitlab.com/publicvoit/orgdown]] URL with brackets without description |   2 |                                                                                                             |
| [[https://gitlab.com/publicvoit/orgdown][OD homepage]] URL with brackets with description                              |   2 |                                                                                                             |
|-----------------------------------------------------------------------------+-----+-------------------------------------------------------------------------------------------------------------|
| Basic table functionality                                                   |   2 |                                                                                                             |
| Table cells with *bold*                                                     |   2 |                                                                                                             |
| Table cells with /italic/                                                   |   2 |                                                                                                             |
| Table cells with _underline_                                                |   2 |                                                                                                             |
| Table cells with +strike through+                                           |   2 |                                                                                                             |
| Table cells with =code=                                                     |   2 |                                                                                                             |
| Table cells with ~commands~                                                 |   2 |                                                                                                             |
| Auto left-aligning of text                                                  |   2 |                                                                                                             |
| Auto right-aligning of numbers like "42.23"                                 |   0 | Numbers are left aligned.                                                                                   |
|-----------------------------------------------------------------------------+-----+-------------------------------------------------------------------------------------------------------------|
| Other syntax elements are interpreted at least as normal text (or better)   |   1 | Caption keywords do not seem to be supported.                                                               |
| Other syntax elements: linebreaks respected (or better)                     |   2 |                                                                                                             |
|-----------------------------------------------------------------------------+-----+-------------------------------------------------------------------------------------------------------------|
| *Sum*                                                                       |  74 |                                                                                                             |
#+TBLFM: @>$2=vsum(@I$2..@>>$2)

