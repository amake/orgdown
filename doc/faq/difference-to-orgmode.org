★ [[doc/FAQs.org][← FAQs overview page]] ★

* What is the difference between Org-mode and Orgdown?

While [[doc/Org-mode.org][Org-mode]] is both, (1) a technical tool for the GNU/Emacs
platform as well as (2) a syntax definition, Orgdown is only a syntax
definition. The golden standard for Org-mode is - and will always be -
GNU/Emacs (or any future formal description of Org-mode). In contrast
to that, any tool may implement various levels of Orgdown very easily.
Using the [[doc/Orgdown-Levels.org][level definitions]], compatibility can be easily checked.

In contrast to [[doc/Org-mode.org][Org-mode]], Orgdown defines /reduced/ sets of Org-mode
syntax elements. The initial set is [[doc/Orgdown-Levels.org][Orgdown level]] one, or "Orgdown1".

In other words, the level of supported syntax elements is: 

Orgdown1 < Orgdown2 < ... ≪ Org-mode

This means that if you do have a tool supporting a decent amount of
Org-mode syntax, it almost certainly supports any existing level of
Orgdown out of the box.

---------------

★ [[doc/FAQs.org][← FAQs overview page]] ★
